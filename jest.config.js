'use strict'

module.exports = {
    moduleFileExtensions: ['js'],
    roots: ['<rootDir>/src/tests'],
    collectCoverage: true,
    collectCoverageFrom: [
        '<rootDir>/src/**/*.{js}',
        '!<rootDir>/src/**/*.test.js',
        '!<rootDir>/src/**/__*__/*',
        '!**/node_modules/**'
    ]
}
