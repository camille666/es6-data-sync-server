const WebSocket = require('ws')
const fs = require('fs')
const moment = require('moment')

class MessageController {
    constructor() {
        this.allClients = new Map()
        this.objValue = ''
        this.objType = ''
    }

    handler(evt) {
        if (this[evt.eventType]) {
            try {
                this[evt.eventType](evt)
            } catch (e) {
                const logName = moment().format('YYYYMMDD') + '.log'
                const logText = `[${moment().format(
                    'hh:mm:ss'
                )}] ${e.toString()} \r\n`
                fs.appendFile('log/' + logName, logText, e => {
                    console.log(e)
                })
            }
        }
    }

    join(event) {
        const wc = event.target
        const msgObj = event.data
        const oldPage = wc.pageInfo.page
        if (!this.allClients.has(msgObj.room)) {
            this.allClients.set(msgObj.room, [])
        }
        const clientArr = this.allClients.get(msgObj.room)
        // 没有房间号，说明是新的连接
        if (!wc.room) {
            clientArr.push(wc)
        }
        // 房间号
        wc.room = msgObj.room
        // 页面唯一id
        wc.pageInfo.pageId = msgObj.pageId
        // 页面url参数
        wc.pageInfo.search = msgObj.search
        // 是否为屏控模式
        wc.pageInfo.control = msgObj.control
        // 页面
        wc.pageInfo.page = msgObj.page
        // 页面发生变化时
        if (oldPage && oldPage !== msgObj.page) {
            this.setNewMaster(msgObj.room, oldPage)
        }
        this.setNewMaster(msgObj.room, msgObj.page)
        // 通知所有大屏
        this.sendMsgAll(msgObj.room, {
            event: 'join',
            pages: this.getPageInfo(msgObj.room)
        })
    }

    // 数据同步
    sync(event) {
        const wc = event.target
        const msgObj = event.data
        const clients = this.getPageClients(wc.room, wc.pageInfo.page)
        clients.forEach(client => {
            if (client !== wc) {
                this.sendMsg(client, msgObj)
            }
        })
    }

    // 研判
    judge(event) {
        const wc = event.target
        const msgObj = event.data
        const clients = this.getRoomClients(wc.room)
        clients.forEach(client => {
            if (client !== wc) {
                this.sendMsg(client, msgObj)
            }
        })
        this.objValue = msgObj.objValue
        this.objType = msgObj.objType
    }

    // 通过http接口研判
    webJudge(req, res) {
        const objValue = req.query.objValue
        const objType = req.query.objType ? req.query.objType : ''
        const room = req.query.room
        if (!objValue || objValue === '' || !room || room === '') {
            res.send({
                success: false,
                data: null,
                msg: '请输入objValue和room'
            })
            return
        }
        // 通知大屏研判
        const event = {
            target: { room },
            eventType: 'judge',
            data: { objValue, objType, event: 'judge' }
        }
        this.judge(event)
        // 返回消息到工作台
        res.send({
            success: true,
            data: null,
            msg: ''
        })
    }

    // 获取当前研判的objValue和objType
    getJudgeInfo(req, res) {
        // 返回消息到工作台
        res.send({
            success: true,
            data: { objType: this.objType, objValue: this.objValue },
            msg: ''
        })
    }

    // 关闭
    close(event) {
        const wc = event.target
        this.removeClient(wc)
        if (wc.pageInfo.master) {
            // 重新选择master
            this.setNewMaster(wc.room, wc.pageInfo.page)
        }
        // 通知所有大屏有屏退出
        this.sendMsgAll(wc.room, {
            event: 'join',
            pages: this.getPageInfo(wc.room)
        })
    }

    // 接收kafka的消息并推送到前端
    kafka() {
        this.sendMsgAll('kafka', { event: 'warn' })
    }

    // 设置主屏
    setNewMaster(room, page) {
        const clients = this.getPageClients(room, page)
        let sel = false
        clients.forEach(wc => {
            wc.pageInfo.master = false
            if (!wc.pageInfo.control && !sel) {
                // 选择一个作为master
                sel = true
                wc.pageInfo.master = true
            }
        })
    }

    // 发送消息
    sendMsg(wc, msg) {
        if (wc.readyState === WebSocket.OPEN) {
            wc.send(JSON.stringify(msg), err => {
                if (err) {
                    console.log(err)
                }
            })
        }
    }

    // 所有客户端发送消息
    sendMsgAll(room, msg) {
        const clients = this.getRoomClients(room)
        clients.forEach(wc => {
            this.sendMsg(wc, msg)
        })
    }

    getPageInfo(room, wc = null) {
        const clients = this.getRoomClients(room)
        let pageInfo = []
        for (const i in clients) {
            const item = clients[i]
            if (wc) {
                if (wc === item) {
                    pageInfo = item.pageInfo
                    break
                }
            } else {
                pageInfo.push(item.pageInfo)
            }
        }
        return pageInfo
    }

    // 同一房间的客户端
    getRoomClients(room) {
        const clients = this.allClients.get(room)
        const clientArr = []
        clients &&
            clients.forEach(wc => {
                if (wc.readyState === WebSocket.OPEN) {
                    clientArr.push(wc)
                }
            })
        return clientArr
    }

    // 同房间相同page的客户端
    getPageClients(room, page) {
        const clients = this.allClients.get(room)
        const clientArr = []
        clients &&
            clients.forEach(wc => {
                if (
                    wc.pageInfo.page === page &&
                    wc.readyState === WebSocket.OPEN
                ) {
                    clientArr.push(wc)
                }
            })
        return clientArr
    }

    // 删除已关闭的client
    removeClient(wc) {
        const room = wc.room
        const clients = this.allClients.get(room)
        for (const i in clients) {
            if (clients[i] === wc) {
                clients.splice(i, 1)
                if (clients.length === 0) {
                    this.allClients.delete(room)
                }
                break
            }
        }
    }
}

module.exports = MessageController
