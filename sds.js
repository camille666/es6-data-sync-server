const config = require('./src/config/config.default.js')

const PORT = config.port
const KAFKA_HOST = config.kafkaHost
const KAFKA_TOPIC = config.kafkaTopic

// http服务
const express = require('express')
const http = require('http')
const app = express()
const server = http.createServer(app)

// websocket服务
const WebSocket = require('ws')
const wss = new WebSocket.Server({ server })

// 消息处理
const MessageController = require('./src/controller/message')
const messageHandler = new MessageController()

// 提供对外的http研判接口
app.get('/sds/judge', (req, res) => {
    messageHandler.webJudge(req, res)
})
// 获取当前研判的objValue,objType
app.get('/sds/getJudgeInfo', (req, res) => {
    messageHandler.getJudgeInfo(req, res)
})

// 健康检查
app.get('/check/startCheck', (req, res) => {
    // 返回消息
    res.send({
        success: true,
        failInfos: {}
    })
})

wss.on('connection', (wc, req) => {
    wc.isAlive = true
    wc.room = ''
    wc.pageInfo = { master: false }
    wc.on('message', msg => {
        const msgObj = JSON.parse(msg)
        messageHandler.handler({
            target: wc,
            data: msgObj,
            eventType: msgObj.event
        })
    })

    wc.on('close', (code, msgRoom) => {
        wc.terminate()
        messageHandler.handler({ target: wc, eventType: 'close' })
    })

    wc.on('pong', heartbeat)

    wc.on('error', err => {
        console.log(err)
    })
})

function heartbeat(e) {
    this.isAlive = true
}

// 心跳检测
setInterval(function ping() {
    wss.clients.forEach(function each(wc) {
        if (wc.isAlive === false) {
            wc.terminate()
            return messageHandler.handler({ target: wc, eventType: 'close' })
        }
        wc.isAlive = false
        wc.ping('', false, true)
    })
}, 3000)

// 当前用于新预警消息提示
if (KAFKA_HOST && KAFKA_HOST !== '') {
    const kafka = require('kafka-node')

    const createConsumer = () => {
        let client = new kafka.KafkaClient({ kafkaHost: KAFKA_HOST })
        let consumer = new kafka.Consumer(client, [{ topic: KAFKA_TOPIC }])

        consumer.addTopics([KAFKA_TOPIC], (err, added) => {
            if (err != null) {
                console.log('addTopics error:', err)
            }
        })

        consumer.on('message', message => {
            // 转发kafka到websocket
            console.log('message:', message)
            messageHandler.handler({
                target: consumer,
                data: 'webWarning',
                eventType: 'kafka'
            })
        })

        consumer.on('error', err => {
            console.log('consumer error:', err)
            consumer.close()
            client.close()
            client = null
            consumer = null
            createConsumer()
        })
    }

    createConsumer()
}

server.listen(PORT, () => {
    console.log('Listening on %d', server.address().port)
})
